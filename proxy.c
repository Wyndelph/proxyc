#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netdb.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/types.h>
#include <pthread.h>

#define SERVADDR "localhost"        // Définition de l'adresse IP d'écoute
#define SERVPORT "0"                // Définition du port d'écoute, 
                                    // si 0 port choisi dynamiquement
#define LISTENLEN 1                 // Taille du tampon de demande de connexion
#define MAXBUFFERLEN 1024
#define MAXHOSTLEN 64
#define MAXPORTLEN 6

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

typedef void* (*PtrFct)(void*);
pthread_t th1;
pthread_attr_t attributs;
const pthread_attr_t * pAttr = &attributs;
bool isConnected = false;      		// booléen indiquant que l'on est bien connecté
int descSockRDV;

/* ENTETES FONCTIONS */
int ConnectionRDVCOM();
int ConnectionSimple(char* server,char* port);
void* traitement(void* param);
ssize_t getMessage(int sock, char *buf ,size_t nbyte); // Effectue la gestion de l'erreur de lecture

/************************/

int main(){    	
	ConnectionRDVCOM();
	close(descSockRDV);
}

int ConnectionRDVCOM()
{
	struct sockaddr_storage myinfo;  // Informations sur la connexion de RDV
	struct sockaddr_storage from;    // Informations sur le client connecté
	socklen_t len;                   	// Variable utilisée pour stocker les 
										// longueurs des structures de socket
	int descSockRDV;                 	// Descripteur de socket de rendez-vous
	int ecode;							// Retour des fonctions
	int descSocktmp;
	struct addrinfo *res,*resPtr;		// Résultat de la fonction getaddrinfo
	struct addrinfo hints;
	char serverName[MAXHOSTLEN];		// Nom de la machine serveur
	char serverPort[MAXPORTLEN];		// Numéro de port
	char serverAddr[MAXHOSTLEN];		// Adresse du serveur

	// Publication de la socket au niveau du système
	// Assignation d'une adresse IP et un numéro de port
	// Mise à zéro de hints
	memset(&hints, 0, sizeof(hints));
	// Initailisation de hints
	hints.ai_flags = AI_PASSIVE;      	// mode serveur, nous allons utiliser la fonction bind
	hints.ai_socktype = SOCK_STREAM;  	// TCP
	hints.ai_family = AF_INET;      	// les adresses IPv4 et IPv6 seront présentées par 
				                        // la fonction getaddrinfo
	// Récupération des informations du serveur
	ecode = getaddrinfo(SERVADDR, SERVPORT, &hints, &res);
	if (ecode) {
		fprintf(stderr,"getaddrinfo: %s\n", gai_strerror(ecode));
		exit(1);
	}
	//Création de la socket IPv4/TCP
	descSockRDV = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
	if (descSockRDV == -1) {
		perror("Erreur creation socket");
		exit(4);
	}
	// Publication de la socket
	ecode = bind(descSockRDV, res->ai_addr, res->ai_addrlen);
	if (ecode == -1) {
		perror("Erreur liaison de la socket de RDV");
		exit(3);
	}
	// Nous n'avons plus besoin de cette liste chainée addrinfo
	freeaddrinfo(res);
	// Récuppération du nom de la machine et du numéro de port pour affichage à l'écran
	len=sizeof(struct sockaddr_storage);
	ecode=getsockname(descSockRDV, (struct sockaddr *) &myinfo, &len);
	if (ecode == -1)
	{
		perror("SERVEUR: getsockname");
		exit(4);
	}
	ecode = getnameinfo((struct sockaddr*)&myinfo, sizeof(myinfo), serverAddr,MAXHOSTLEN, 
                         serverPort, MAXPORTLEN, NI_NUMERICHOST | NI_NUMERICSERV);
	if (ecode != 0) {
		fprintf(stderr, "error in getnameinfo: %s\n", gai_strerror(ecode));
		exit(4);
	}
	printf("L'adresse d'ecoute est: %s\n", serverAddr);
	printf("Le port d'ecoute est: %s\n", serverPort);
	// Definition de la taille du tampon contenant les demandes de connexion
	ecode = listen(descSockRDV, LISTENLEN);
	if (ecode == -1) {
		perror("Erreur initialisation buffer d'écoute");
		exit(5);
	}
	// Initialisation attributs thread
    ecode = pthread_attr_init(&attributs);
    if(ecode!=0){
        perror("Echec pthread_attr_init\n");
        exit(4);
    } 
    ecode = pthread_attr_setdetachstate(&attributs,PTHREAD_CREATE_DETACHED);
    if(ecode!=0){
        perror("Echec détach\n");
        exit(5);
    }
    while(1){
        printf(ANSI_COLOR_CYAN "---- Attente d'un client ----\n");
        descSocktmp = accept(descSockRDV, (struct sockaddr *) &from, &len);
        if (descSocktmp == -1){
            perror("Erreur accept\n");
            exit(6);
        }
        ecode= pthread_create(&th1, pAttr, traitement, &descSocktmp);
        if(ecode!=0){
            perror("Erreur création thread");
            exit(7);
        }
    }
	return descSocktmp;
}

int ConnectionSimple(char* server,char* port)
{
	struct sockaddr_storage myinfo;  	// Informations sur la connexion de RDV
	struct sockaddr_storage from;    	// Informations sur le client connecté
	char serverName[MAXHOSTLEN];		// Nom de la machine serveur
	char serverPort[MAXPORTLEN];		// Numéro de port
	char serverAddr[MAXHOSTLEN];		// Adresse du serveur
	socklen_t len;                   	// Variable utilisée pour stocker les 
										// longueurs des structures de socket
	int ecode;							// Retour des fonctions
	struct addrinfo *res,*resPtr;		// Résultat de la fonction getaddrinfo
	struct addrinfo hints;
	int descSock;

    strcpy(serverName, server);
    serverName[MAXHOSTLEN-1] = '\0';
    strcpy(serverPort, port);
    serverPort[MAXPORTLEN-1] = '\0';

    // Initailisation de hints
	memset(&hints, 0, sizeof(hints)); 
	hints.ai_socktype = SOCK_STREAM;  	// TCP
	hints.ai_family = AF_INET;     		// Voir sujet : connexion au serveur uniquement par IPv4 
				                    	// la fonction getaddrinfo
	//Récupération des informations sur le serveur
	ecode = getaddrinfo(serverName,serverPort,&hints,&res);
	if (ecode){
		fprintf(stderr,"getaddrinfo: %s\n", gai_strerror(ecode));
		exit(1);
	}
	resPtr = res;
	isConnected = false;
	while(!isConnected && resPtr!=NULL){
		//Création de la socket IPv4/TCP
		descSock = socket(resPtr->ai_family, resPtr->ai_socktype, resPtr->ai_protocol);
		if (descSock == -1) {
			perror("Erreur creation socket");
			exit(2);
		}
  		//Connexion au serveur
		ecode = connect(descSock, resPtr->ai_addr, resPtr->ai_addrlen);
		if (ecode == -1) {
			resPtr = resPtr->ai_next;    		
			close(descSock);	
		}
		// On a pu se connecter
		else isConnected = true;
	}
	freeaddrinfo(res);
	if (!isConnected){
		perror("Connexion impossible");
		exit(2);
	}
	printf("Connexion établie.\n");
	return descSock;
}

void* traitement(void* param)
{
	int descSockCOM = (*((int*)param));	// Récupération du socket de communication entre client / proxy

	int ecode;							// Retour des fonctions
	char adrDataClient[MAXHOSTLEN];		// Ip extrait de la commande PORT
	char portDataClient[MAXPORTLEN]; 	// port extrait de la commande PORT
	char adrDataServeur[MAXHOSTLEN];
	char portDataServeur[MAXPORTLEN];
	char buffer[MAXBUFFERLEN];     		// Buffer stockant les messages entre 
										// le client et le serveur
	struct addrinfo *res,*resPtr;		// Résultat de la fonction getaddrinfo
	struct addrinfo hints;
	char serveur[50];
	char commande[25];
	int descSockData1;					// Descripteur du socket de données entre le client et le proxy
	int descSockData2;					// Descripteur du socket de données entre le proxy et le serveur	
	int descSockPS;
	int ip1,ip2,ip3,ip4,pc1,pc2,ips1,ips2,ips3,ips4,ps1,ps2;
	/* GESTION DE L'USER */
	printf("---- Envoi du message 220 de demande de login du client ----\n");
	strcpy(buffer, "220 Demande de l'identifiant\n");
	write(descSockCOM, buffer, strlen(buffer));
	printf("---- Reception de la réponse du login par le client ----\n");
	ecode = getMessage(descSockCOM, buffer, sizeof(buffer)-1);
	printf("---- Extraction de la commande user et du serveur ----\n");
	sscanf(buffer,"%[^@]@%s", commande, serveur);
	strcat(commande,"\n");
	/* CONNEXION CLIENT-PROXY */
	printf("---- Connexion au serveur '%s' sur le port 21 ----\n", serveur);
	descSockPS = ConnectionSimple(serveur,"21");
	/* GESTION DU PASSWORD */
	printf("---- Reception du message 220 de demande de login du serveur ----\n");
	ecode = getMessage(descSockPS, buffer, sizeof(buffer)-1);
	printf("---- Transmission du login du client vers le serveur ----\n");
	strcpy(buffer, commande);
	write(descSockPS, buffer, strlen(buffer));
	printf("---- Reception du message 331 de demande de password du serveur ----\n");
	ecode = getMessage(descSockPS, buffer, sizeof(buffer)-1);
	printf("---- Transmission du message 331 de demande de password du serveur vers le client ----\n");
	write(descSockCOM, buffer, strlen(buffer));
	printf("---- Reception de la réponse du password par le client ----\n");
	ecode = getMessage(descSockCOM, buffer, sizeof(buffer)-1);
	printf("---- Transmission du password du client vers le serveur ----\n");
	write(descSockPS, buffer, strlen(buffer));
	/* CONNEXION ACCEPTEE */
	printf("---- Reception du message 230 d'acceptation de connexion du serveur ----\n");
	ecode = getMessage(descSockPS, buffer, sizeof(buffer)-1);
	printf("---- Transmission du message 230 d'acceptation de connexion du serveur vers le client----\n");
	write(descSockCOM, buffer, strlen(buffer));
	/* SYST */
	printf("---- Reception du message SYST du demande du systeme du client ----\n");
	ecode = getMessage(descSockCOM, buffer, sizeof(buffer)-1);
	printf("---- Transmission du message SYST du client vers le serveur ----\n");
	write(descSockPS,buffer,strlen(buffer));
	printf("---- Reception du message 215 du serveur ----\n");
	ecode = getMessage(descSockPS, buffer, sizeof(buffer)-1);
	printf("---- Transmission du message 215 de description du systeme du serveur vers le client ----\n");
	write(descSockCOM,buffer,strlen(buffer));
	/* RECEPTION COMMANDE */
	printf("---- Reception de la première commande émise par le client ----\n");
	ecode = getMessage(descSockCOM, buffer, sizeof(buffer)-1);
	sscanf(buffer, "%s %*", commande);
	while (strncmp(commande,"QUIT",4) != 0)
	{
		if(strncmp(commande,"PORT",4) == 0)
		{
			/* PORT */
			printf("---- Reception du message PORT du client ----\n");
			printf("---- Extraction de l'adresse IP et du PORT du client ----\n");
			sscanf(buffer, "PORT %d,%d,%d,%d,%d,%d",&ip1,&ip2,&ip3,&ip4,&pc1,&pc2);
			// printf("%d,%d,%d,%d,%d,%d\n",ip1,ip2,ip3,ip4,pc1,pc2); CHECK PORT EXTRACT
			sprintf(adrDataClient,"%d.%d.%d.%d",ip1,ip2,ip3,ip4);
			sprintf(portDataClient,"%d",pc1*256+pc2);
			printf("---- Envoi du message 200 de bonne reception de la commande PORT au client ----\n");
			strcpy(buffer, "200 PORT command successful. \n");
			write(descSockCOM,buffer,strlen(buffer));
			printf("---- Réception de la commande du client ----\n");
			ecode = read(descSockCOM, commande, sizeof(commande)-1);
			if (ecode == -1) {perror("Problème de lecture\n"); exit(3);}
			commande[ecode] = '\0';
		}
		else
		{
			printf("---- Transmission de la commande client %s au serveur ----\n",commande);
			write(descSockPS,buffer,strlen(buffer));
			printf("---- Reception de la réponse serveur du %s ----\n",commande);
			ecode = getMessage(descSockPS, buffer, sizeof(buffer)-1);
			printf("---- Transmission de la réponse au client ----\n");
			write(descSockCOM,buffer,strlen(buffer));
		}
		if(strncmp(commande,"LIST\n",4) == 0)
		{
			printf("---- Ouverture de la connexion de données du proxy vers le client ----\n");
			descSockData1 = ConnectionSimple(adrDataClient,portDataClient);
			printf("---- Envoi du message PASV au serveur pour récupérer la commande PORT du serveur ----\n");
			strcpy(buffer, "PASV\n");
			write(descSockPS,buffer,strlen(buffer));
			printf("---- Reception de la commande PORT du serveur ----\n");
			ecode = getMessage(descSockPS, buffer, sizeof(buffer)-1);
			printf("---- Extraction de l'IP et du PORT du serveur ----\n");
			sscanf(buffer,"%*[^(](%d,%d,%d,%d,%d,%d",&ips1,&ips2,&ips3,&ips4,&ps1,&ps2);
			sprintf(adrDataServeur,"%d.%d.%d.%d",ips1,ips2,ips3,ips4);
			sprintf(portDataServeur,"%d",ps1*256+ps2);
			printf("---- Ouverture de la connexion de données du proxy vers le serveur ----\n");
			descSockData2 = ConnectionSimple(adrDataServeur,portDataServeur);
			printf("---- Transmission du message LIST du client au serveur ----\n");
			write(descSockPS, commande, strlen(commande));
			printf("---- Reception du message 150 pour la réponse à la commande LIST du serveur ----\n");
			ecode = getMessage(descSockPS, buffer, sizeof(buffer)-1);
			printf("---- Transmission du message 150 du serveur au client ----\n");
			write(descSockCOM,buffer,strlen(buffer));
			printf("---- Reception LIST ----\n");
			ecode = getMessage(descSockData2, buffer, sizeof(buffer)-1);
			while (ecode != 0) {
				write(descSockData1, buffer,ecode);
				ecode = getMessage(descSockData2, buffer, sizeof(buffer)-1);
			}
			printf("---- Fermeture des sockets de données ----\n");
			close(descSockData1);
			close(descSockData2);
			printf("---- Reception de la fin de LIST par le serveur ----\n");
			ecode = getMessage(descSockPS, buffer, sizeof(buffer)-1);
			printf("---- Transmission de la fin de la LIST du serveur vers le client ----\n");
			write(descSockCOM, buffer,ecode);	
		}
		printf("---- Relecture de la commande envoyée par le client ----\n");
		ecode = getMessage(descSockCOM, buffer, sizeof(buffer)-1);
		sscanf(buffer, "%s %*", commande);	
	}
	printf("FIN.\n");	
	close(descSockPS);
	close(descSockCOM);
}
ssize_t getMessage(int sock, char *buf ,size_t nbyte){
        ssize_t ecode = read(sock, buf, nbyte);
        if(ecode== -1) {perror("Problème de lecture\n"); exit(3);}
        buf[ecode] = '\0';
        return ecode;
}
